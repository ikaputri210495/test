-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Sep 2020 pada 06.05
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sireka`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `harga`
--

CREATE TABLE `harga` (
  `id` int(25) NOT NULL,
  `id_pelanggan` int(25) NOT NULL,
  `kontrak` varchar(255) NOT NULL,
  `harga` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0 => Aktif,\r\n1 => Tidak Aktif',
  `idt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `harga`
--

INSERT INTO `harga` (`id`, `id_pelanggan`, `kontrak`, `harga`, `status`, `idt`) VALUES
(15, 18, 'perusahaan/2020', '890', '0', '0000-00-00 00:00:00'),
(16, 19, 'plta/2020/001', '828.62', '0', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `history`
--

CREATE TABLE `history` (
  `id` int(25) NOT NULL,
  `id_pelanggan` int(25) NOT NULL,
  `id_titik` int(25) NOT NULL,
  `meteran` varchar(255) NOT NULL,
  `id_titik_pembanding` int(25) NOT NULL,
  `meteran_pembanding` varchar(255) NOT NULL,
  `faktor_kali` varchar(25) NOT NULL,
  `faktor_kali_pembanding` varchar(25) NOT NULL,
  `total_harga` varchar(255) NOT NULL,
  `idt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pelanggan`
--

CREATE TABLE `pelanggan` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `tlp` varchar(25) NOT NULL,
  `email` varchar(255) NOT NULL,
  `idt` datetime NOT NULL DEFAULT current_timestamp(),
  `status` enum('0','1') NOT NULL COMMENT '0 => Active,\r\n1 => Deleted'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pelanggan`
--

INSERT INTO `pelanggan` (`id`, `nama`, `alamat`, `tlp`, `email`, `idt`, `status`) VALUES
(19, 'CV. Mitra Muda Manggala (Cilulumpang + Cinangka Atas)', 'Jl. Ir.H. Djuanda No.03 - Cinangka - Jatiluhur', '021123321', '021123321', '2020-09-01 10:56:25', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `titik`
--

CREATE TABLE `titik` (
  `id` int(25) NOT NULL,
  `id_pelanggan` int(25) NOT NULL,
  `nama_titik` varchar(255) NOT NULL,
  `caption` text NOT NULL,
  `merk` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `nomor` varchar(255) NOT NULL,
  `faktor_kali` varchar(25) NOT NULL,
  `qr_code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `titik`
--

INSERT INTO `titik` (`id`, `id_pelanggan`, `nama_titik`, `caption`, `merk`, `type`, `nomor`, `faktor_kali`, `qr_code`) VALUES
(9, 19, 'TRAFO I', 'CT ; 400/5 Amper kWh meter mekanik 3 phasa 4 kawat', 'ITRON', 'C116W-1/6R1', 'M061500164', '80', '9.png'),
(10, 19, 'TRAFO II', 'CT ; 400/5 Amper kWh meter mekanik 3 phasa 4 kawat', 'ITRON', 'C116W-1/6R1', 'M061500165', '80', '10.png'),
(11, 19, 'TRAFO LAUNDRY - CINANGKA ATAS', 'CT ; 100/5 Amper kWh meter mekanik 3 phasa 4 kawat', 'ITRON', 'C116W-1/6R1', 'M061500184', '20', '11.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `titik_pembanding`
--

CREATE TABLE `titik_pembanding` (
  `id_pembanding` int(25) NOT NULL,
  `id_titik` int(25) NOT NULL,
  `id_pelanggan` int(25) DEFAULT NULL,
  `merk_pembanding` varchar(255) NOT NULL,
  `type_pembanding` varchar(255) NOT NULL,
  `nomor_pembanding` varchar(255) NOT NULL,
  `faktor_kali_pembanding` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `titik_pembanding`
--

INSERT INTO `titik_pembanding` (`id_pembanding`, `id_titik`, `id_pelanggan`, `merk_pembanding`, `type_pembanding`, `nomor_pembanding`, `faktor_kali_pembanding`) VALUES
(9, 9, 19, 'Fuji Electric', 'FF 23 HT1', '153260', '80'),
(10, 10, 19, 'Fuji Dharma Electric', 'FF 23 H', '000345024', '80'),
(11, 11, 19, 'ACTARIS', 'C116W-1/6R1', 'M0601174', '20');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(25) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) NOT NULL,
  `role` enum('root','petugas') NOT NULL,
  `status` enum('0','1') NOT NULL COMMENT '0 => User Aktif,\r\n1 => User Dihapus',
  `date_added` datetime NOT NULL DEFAULT current_timestamp(),
  `date_modified` datetime DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `fullname`, `username`, `password`, `salt`, `role`, `status`, `date_added`, `date_modified`) VALUES
(1, 'Admin SIREKA', 'admin', '332c6766d86cc7d8a413e74667d0a9b0553d167e121f074e04a75aa77d0be37e5cf5ebe16e9f0ad7a70762828aebd65e3b0eecc82de2250633c60f54d61a8972', '6094328d52d3a3730d2c1c492863179470f2449e115eecd01034847915d86d2910c08bd1d4a360642da5ecfc1f9aa3f3e327c6ff16f46e46ca114cbe8cc97e48', 'root', '0', '2020-08-24 09:21:20', NULL);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `harga`
--
ALTER TABLE `harga`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `history`
--
ALTER TABLE `history`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pelanggan`
--
ALTER TABLE `pelanggan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `titik`
--
ALTER TABLE `titik`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `titik_pembanding`
--
ALTER TABLE `titik_pembanding`
  ADD PRIMARY KEY (`id_pembanding`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `harga`
--
ALTER TABLE `harga`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT untuk tabel `history`
--
ALTER TABLE `history`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `pelanggan`
--
ALTER TABLE `pelanggan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `titik`
--
ALTER TABLE `titik`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `titik_pembanding`
--
ALTER TABLE `titik_pembanding`
  MODIFY `id_pembanding` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(25) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
