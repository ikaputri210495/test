<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'third_party/REST_Controller.php';
require APPPATH . 'third_party/Format.php';

use Restserver\Libraries\REST_Controller;

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
*/

class Api extends REST_Controller
{
    public function __construct() {
        parent::__construct();
        // Load these helper to create JWT tokens
        $this->load->helper(['jwt', 'authorization']);  
        // Load model
        $this->load->model('Api_model', 'api');
        // Load Library
        $this->load->library('pdf');
        // Set timezone
        date_default_timezone_set("Asia/Jakarta");
    }

    // Coba
    public function cobain_get() {
        $this->response([
            'status'    => parent::HTTP_OK,
            'msg'       => 'Halloooo :)',
        ], REST_Controller::HTTP_OK);
    }

    public function scan_cek_post() {
        $date = date('Y-m');
        $id_pelanggan = $this->post('id_pelanggan');
        $id_titik = $this->post('id_titik');
        
        $getDataPelanggan = $this->api->get_pelanggan_by_id($id_pelanggan)->row_array();
        $cekTitik = $this->api->cek_titik($id_pelanggan, $id_titik, $date)->num_rows();

        if ($cekTitik >= 1) {
            $this->response([
                'status'            => parent::HTTP_OK,
                'code'              => false,
                'msg'               => 'Titik Sudah Terisi',
                'data_pelanggan'    => $getDataPelanggan
            ], REST_Controller::HTTP_OK);
        } else {
            $cekJumlahTitik     = $this->api->cek_jumlah_titik_by_id_pelanggan($id_pelanggan)->num_rows();
            $jmlTitikPerPeriode = $this->api->cek_titik_periode_by_id_Pelanggan($id_pelanggan, $date)->num_rows();

            if ($jmlTitikPerPeriode < 1 && $cekJumlahTitik != 1) {
                $this->response([
                    'status'    => parent::HTTP_OK,
                    'code'      => true,
                    'signature' => 1,
                    'msg'       => 'BA Belum Bisa Terbit',
                    'data_pelanggan'    => $getDataPelanggan
                ], REST_Controller::HTTP_OK);
            } else {
                if ($jmlTitikPerPeriode == $cekJumlahTitik-1) {
                    $this->response([
                        'status'    => parent::HTTP_OK,
                        'code'      => true,
                        'signature' => 0,
                        'msg'       => 'BA Sudah Bisa Terbit',
                        'data_pelanggan'    => $getDataPelanggan
                    ], REST_Controller::HTTP_OK);
                } else {
                    $this->response([
                        'status'    => parent::HTTP_OK,
                        'code'      => true,
                        'signature' => 1,
                        'msg'       => 'BA Belum Bisa Terbit',
                        'data_pelanggan'    => $getDataPelanggan
                    ], REST_Controller::HTTP_OK);
                }
            }
        }
    }
}


?>