<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    public function __construct() {
        parent::__construct();

        // Load model
        $this->load->model('Dashboard_model', 'dashboard');
        $this->load->model('Crud_model', 'crud');
        // Set timezone
        date_default_timezone_set("Asia/Jakarta");
    }

    // --------------------------------------------------------------------------------------------
    // Tampilan Page
    // --------------------------------------------------------------------------------------------
    
    function index() {
        $data['title'] = 'Dashboard';

        if ($this->session->userdata('user_id') != '' && $this->session->userdata('user_role') == 'root') {
            $data['add_js'] = [
                "assets/www.amcharts.com/lib/3/amcharts.js",
                "assets/www.amcharts.com/lib/3/serial.js",
                "assets/www.amcharts.com/lib/3/radar.js",
                "assets/www.amcharts.com/lib/3/pie.js",
                "assets/www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js",
                "assets/www.amcharts.com/lib/3/plugins/animate/animate.min.js",
                "assets/www.amcharts.com/lib/3/plugins/export/export.min.js",
                "assets/www.amcharts.com/lib/3/themes/light.js",
                "assets/action_js/admin/dashboard.js"
            ];
            $this->render_page('admin/dashboard', $data);
        } elseif ($this->session->userdata('user_id') == '') {
            redirect('webbackend/login');
        }
    }
}

?>