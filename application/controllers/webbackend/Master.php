<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/phpqrcode/qrlib.php';

class Master extends MY_Controller
{
    public function __construct() {
        parent::__construct();

        // Load model
        $this->load->model('Master_model', 'master');
        $this->load->model('Crud_model', 'crud');
        // Set timezone
        date_default_timezone_set("Asia/Jakarta");
    }

    // --------------------------------------------------------------------------------------------
    // Tampilan Page
    // --------------------------------------------------------------------------------------------
    
    function index() {
        $data['title'] = 'Master Data';

        if ($this->session->userdata('user_id') != '' && $this->session->userdata('user_role') == 'root') {
            $data['add_js'] = [
                "assets/www.amcharts.com/lib/3/amcharts.js",
                "assets/www.amcharts.com/lib/3/serial.js",
                "assets/www.amcharts.com/lib/3/radar.js",
                "assets/www.amcharts.com/lib/3/pie.js",
                "assets/www.amcharts.com/lib/3/plugins/tools/polarScatter/polarScatter.min.js",
                "assets/www.amcharts.com/lib/3/plugins/animate/animate.min.js",
                "assets/www.amcharts.com/lib/3/plugins/export/export.min.js",
                "assets/www.amcharts.com/lib/3/themes/light.js",
                "assets/action_js/admin/master.js"
            ];
            $this->render_page('admin/master', $data);
        } elseif ($this->session->userdata('user_id') == '') {
            redirect('webbackend/login');
        }
    }

    function create() {
        $data['title'] = 'Tambah Pelanggan';

        if ($this->session->userdata('user_id') != '' && $this->session->userdata('user_role') == 'root') {
            $data['add_css'] = [
                "assets/vendors/custom/datatables/datatables.bundle.css"
            ];
            $data['add_js'] = [
                "assets/vendors/custom/datatables/datatables.bundle.js",
                "assets/action_js/admin/master_create.js"
            ];
            $this->render_page('admin/master_create', $data);
        } elseif ($this->session->userdata('user_id') == '') {
            redirect('webbackend/login');
        }
    }

    function list_pelanggan() {
        $data['title'] = 'List Pelanggan';

        if ($this->session->userdata('user_id') != '' && $this->session->userdata('user_role') == 'root') {
            $data['add_css'] = [
                "assets/vendors/custom/datatables/datatables.bundle.css"
            ];
            $data['add_js'] = [
                "assets/vendors/custom/datatables/datatables.bundle.js",
                "assets/action_js/admin/master_list_pelanggan.js"
            ];
            $this->render_page('admin/master_list_pelanggan', $data);
        } elseif ($this->session->userdata('user_id') == '') {
            redirect('webbackend/login');
        }
    }

    // --------------------------------------------------------------------------------------------
    // Proses
    // --------------------------------------------------------------------------------------------

    public function data_table_titik($id_perusahaan) {
        $data = $this->master->list_data_pelanggan($id_perusahaan);
        header('Content-Type: application/json');
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function data_table_pelanggan() {
        $data = $this->master->list_data_customers();
        header('Content-Type: application/json');
        echo json_encode($data, JSON_PRETTY_PRINT);
    }

    public function insert_data_perusahaan() {
        $nama_perusahaan = $this->input->post('nama_perusahaan');
        $email_perusahaan = $this->input->post('email_perusahaan');
        $alamat_perusahaan = $this->input->post('alamat_perusahaan');
        $tlp_perusahaan = $this->input->post('tlp_perusahaan');

        $data = [
            'nama'      => $nama_perusahaan,
            'alamat'    => $alamat_perusahaan,
            'tlp'       => $tlp_perusahaan,
            'email'     => $tlp_perusahaan
        ];

        $id_perusahaan = $this->crud->save_with_id($data, 'pelanggan');

        if ($id_perusahaan > 0) {
            $response = [
                'status'        => 200,
                'msg'           => 'berhasil',
                'id_perusahaan' => $id_perusahaan
            ];
        } else {
            $response = [
                'status'        => 500,
                'msg'           => 'gagal'
            ];
        }

        header('Content-Type: application/json');
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    public function insert_data_kontrak() {
        $id_perusahaan = $this->input->post('id_perusahaan');
        $nomor_kontrak = $this->input->post('nomor_kontrak');
        $harga_kontrak = $this->input->post('harga_kontrak');

        $data = [
            'id_pelanggan'      => $id_perusahaan,
            'kontrak'           => $nomor_kontrak,
            'harga'             => $harga_kontrak
        ];

        if ($this->crud->save($data, 'harga') > 0) {
            $response = [
                'status'        => 200,
                'msg'           => 'berhasil'
            ];
        } else {
            $response = [
                'status'        => 500,
                'msg'           => 'gagal'
            ];
        }

        header('Content-Type: application/json');
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    public function insert_data_titik() {
        $id_perusahaan              = $this->input->post('id_perusahaan_titik');
        $nama_titik_pengukur        = $this->input->post('nama_titik_pengukur');
        $caption_titik_pengukur     = $this->input->post('caption_titik_pengukur');
        $merk_titik_pengukur        = $this->input->post('merk_titik_pengukur');
        $type_titik_pengukur        = $this->input->post('type_titik_pengukur');
        $nomor_titik_pengukur       = $this->input->post('nomor_titik_pengukur');
        $faktor_kali_titik_pengukur = $this->input->post('faktor_kali_titik_pengukur');

        // Data titik pengukur
        $data_pengukur = [
            'id_pelanggan'      => $id_perusahaan,
            'nama_titik'        => $nama_titik_pengukur,
            'caption'           => $caption_titik_pengukur,
            'merk'              => $merk_titik_pengukur,
            'type'              => $type_titik_pengukur,
            'nomor'             => $nomor_titik_pengukur,
            'faktor_kali'       => $faktor_kali_titik_pengukur
        ];

        $id_titik_pengukur = $this->crud->save_with_id($data_pengukur, 'titik');

        if ($id_titik_pengukur > 0) {
            $merk_titik_pembanding          = $this->input->post('merk_titik_pembanding');
            $type_titik_pembanding          = $this->input->post('type_titik_pembanding');
            $nomor_titik_pembanding         = $this->input->post('nomor_titik_pembanding');
            $faktor_kali_titik_pembanding   = $this->input->post('faktor_kali_titik_pembanding');

            $data_pembanding = [
                'id_titik'                  => $id_titik_pengukur,
                'id_pelanggan'              => $id_perusahaan,
                'merk_pembanding'           => $merk_titik_pembanding,
                'type_pembanding'           => $type_titik_pembanding,
                'nomor_pembanding'          => $nomor_titik_pembanding,
                'faktor_kali_pembanding'    => $faktor_kali_titik_pembanding
            ];
            $id_titik_pembanding = $this->crud->save_with_id($data_pembanding, 'titik_pembanding');

            if ($id_titik_pembanding > 0) {
                $per = $this->master->get_perusahaan($id_perusahaan);
                $this->createQR($id_perusahaan, $id_titik_pengukur, $id_titik_pembanding, $faktor_kali_titik_pengukur, $faktor_kali_titik_pembanding);

                $response = [
                    'status'        => 200,
                    'msg'           => 'berhasil'
                ];
            } else {
                $response = [
                    'status'        => 500,
                    'msg'           => 'gagal'
                ];
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response, JSON_PRETTY_PRINT);
    }

    function createQR($id_perusahaan, $id_titik_pengukur, $id_titik_pembanding, $faktor_kali_titik_pengukur, $faktor_kali_titik_pembanding) {
        $tempdir = 'assets/qr/';
        //ambil logo
        $logopath= base_url().'assets/logoqr/logo1.png';
        $isi = [
            'id_perusahaan'                 => $id_perusahaan,
            'id_titik_pengukur'             => $id_titik_pengukur,
            'id_titik_pembanding'           => $id_titik_pembanding,
            'faktor_kali_titik_pengukur'    => $faktor_kali_titik_pengukur,
            'faktor_kali_titik_pembanding'  => $faktor_kali_titik_pembanding
        ];
        //isi qrcode jika di scan
        $codeContents = json_encode($isi);  //Nanti kemungkinan disiisi dengan API

        //simpan file qrcode
        QRcode::png($codeContents, $tempdir.$id_titik_pengukur.'.png', QR_ECLEVEL_H, 10,7);

        // ambil file qrcode
        $QR = imagecreatefrompng($tempdir.$id_titik_pengukur.'.png');

        // memulai menggambar logo dalam file qrcode
        $logo = imagecreatefromstring(file_get_contents($logopath));

        imagecolortransparent($logo , imagecolorallocatealpha($logo , 0, 0, 0, 127));
        imagealphablending($logo , false);
        imagesavealpha($logo , true);

        $QR_width = imagesx($QR);
        $QR_height = imagesy($QR);

        $logo_width = imagesx($logo);
        $logo_height = imagesy($logo);

        // Scale logo to fit in the QR Code
        $logo_qr_width = $QR_width/7;
        $scale = $logo_width/$logo_qr_width;
        $logo_qr_height = $logo_height/$scale;

        imagecopyresampled($QR, $logo, $QR_width/2.4, $QR_height/2.4, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);

        // Simpan kode QR lagi, dengan logo di atasnya
        imagepng($QR,$tempdir.$id_titik_pengukur.'.png');

        // save to db
        $data_file = [
            'qr_code'   => $id_titik_pengukur.'.png'
        ];

        $where = ['id' => $id_titik_pengukur];

        $this->crud->update($data_file, $where, 'titik');
    }
}

?>