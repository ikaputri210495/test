<?php

class Main extends CI_Controller
{
    public function __construct() {
		parent::__construct();

		// Load model
		$this->load->model('Main_model', 'main_model');
		$this->load->model('Crud_model', 'crud');
    }

    public function login() {
		// Check if you have login
		if ($this->session->userdata('user_id') != '' && $this->session->userdata('user_role') != 'root') {
			redirect(base_url() . 'webbackend/dashboard');
		}
		$data['title'] = 'Login';
		$this->load->view("login", $data);
	}

	public function login_validation() {
		// Extract user data from POST request
        $username = $this->input->post('username');
		$password = $this->input->post('password');
		
		$row = $this->main_model->check_users($username);

		if ($row->num_rows() > 0) {
			$fetch = $row->row_array();
			if ($fetch['role'] == 'root') {
				// Hashing password template
				$password_h	= hash('sha512', $password);
				$hash     	= hash('sha512', $password_h . $fetch['salt']);
				if ($hash == $fetch['password']) {
					// Set session to array
					$session_data = array(
						'user_id' 		=> $fetch['id'],
						'user_fullname' => $fetch['fullname'],
						'user_username' => $fetch['username'],
						'user_role'     => $fetch['role'],
						'user_status'	=> $fetch['status']
					);
	
					$this->session->set_userdata($session_data);
	
					$response = [
						'msg'   => 'Berhasil login',
						'url'   => base_url().'webbackend/dashboard'
					];
				} else {
					$response = [
						'msg'   => 'Gagal login'
					];
				}
			}
		} else {
			$response = [
				'msg'   => 'Tidak terdaftar'
			];
		}

		header('Content-Type: application/json');
        echo json_encode($response, JSON_PRETTY_PRINT);
	}

	public function logout() {
		$id_user = $this->session->userdata('user_id');
		$row = $this->main_model->check_user_by_id($id_user)->row_array();

		if ($row['role'] == 'root') {
			$this->session->sess_destroy();
			session_destroy();
			redirect(base_url() . 'webbackend/login');
		}
	}
}


?>