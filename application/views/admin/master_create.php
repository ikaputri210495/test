<div class="m-wizard m-wizard--2 m-wizard--success" id="m_wizard">
    <!--begin: Message container -->
    <div class="m-portlet__padding-x">
        <!-- Here you can put a message or alert -->
    </div>

    <div class="m-wizard__head m-portlet__padding-x">
        <!--begin: Form Wizard Progress -->
        <div class="m-wizard__progress">
            <div class="progress">
                <div class="progress-bar" id="progress-pross" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div> <!-- style="width: 100%;" -->
            </div>
        </div>
        <!--end: Form Wizard Progress -->
        <div class="m-wizard__nav">
            <div class="m-wizard__steps">
                <div class="m-wizard__step m-wizard__step--current" id="m_wizard_form_step_1" m-wizard-target="m_wizard_form_step_1">
                    <a href="#" class="m-wizard__step-number">
                        <span><i class="fa  flaticon-avatar"></i></span>
                    </a>
                    <div class="m-wizard__step-info">
                        <div class="m-wizard__step-title">
                            1. Pelanggan
                        </div>
                        <div class="m-wizard__step-desc">
                            Detail general data pelanggan
                        </div>
                    </div>
                </div>
                <div class="m-wizard__step" id="m_wizard_form_step_2" m-wizard-target="m_wizard_form_step_2">
                    <a href="#" class="m-wizard__step-number">
                        <span><i class="fa  flaticon-file-2"></i></span>
                    </a>
                    <div class="m-wizard__step-info">
                        <div class="m-wizard__step-title">
                            2. Kontrak
                        </div>
                        <div class="m-wizard__step-desc">
                            Informasi seputar kontrak<br>
                            dan harga per kWh
                        </div>
                    </div>
                </div>
                <div class="m-wizard__step" id="m_wizard_form_step_3" m-wizard-target="m_wizard_form_step_3">
                    <a href="#" class="m-wizard__step-number">
                        <span><i class="fa  flaticon-map-location"></i></span>
                    </a>
                    <div class="m-wizard__step-info">
                        <div class="m-wizard__step-title">
                            3. Titik
                        </div>
                        <div class="m-wizard__step-desc">
                            Informasi titik alat kWh<br>
                            dan titik pembanding
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="m-portlet m-portlet--success m-portlet--head-solid-bg" id="group-pelanggan">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Buat pelanggan Baru
                </h3>
            </div>
        </div>
    </div>
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right" id="formPelanggan">
        <div class="m-portlet__body">
            <div class="m-form__content">
                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_1_msg">
                    <div class="m-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="m-alert__text">
                        Field bertanda * wajib diisi. Harap perhatikan inputan anda.
                    </div>
                    <div class="m-alert__close">
                        <button type="button" class="close" data-close="alert" aria-label="Close">
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-2 col-sm-12">Nama Perusahaan <span class="text-danger">*</span></label>
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <input type="text" class="form-control m-input" name="nama_perusahaan" placeholder="Masukan nama perusahaan" autocomplete="off">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-2 col-sm-12">Email <span class="text-danger">*</span></label>
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <input type="email" class="form-control m-input" name="email_perusahaan" placeholder="Masukan email perusahaan yang valid" autocomplete="off">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-2 col-sm-12">Alamat</label>
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <input type="text" class="form-control m-input" name="alamat_perusahaan" placeholder="Detail alamat perusahaan" autocomplete="off">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-2 col-sm-12">Telphone</label>
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <input type="text" class="form-control m-input" name="tlp_perusahaan" placeholder="Masukan telphone perusahaan" autocomplete="off">
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <button type="submit" class="btn btn-success pull-right" id="savePelanggan"> <i class="la la-save mr-2"></i>Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--end::Form-->
</div>

<div class="m-portlet m-portlet--success m-portlet--head-solid-bg" id="group-kontrak">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Kontrak Pelanggan
                </h3>
            </div>
        </div>
    </div>
    <!--begin::Form-->
    <form class="m-form m-form--fit m-form--label-align-right" id="formKontrak">
        <div class="m-portlet__body">
            <div class="m-form__content">
                <div class="m-alert m-alert--icon alert alert-danger m--hide" role="alert" id="m_form_2_msg">
                    <div class="m-alert__icon">
                        <i class="la la-warning"></i>
                    </div>
                    <div class="m-alert__text">
                        Field bertanda * wajib diisi. Harap perhatikan inputan anda.
                    </div>
                    <div class="m-alert__close">
                        <button type="button" class="close" data-close="alert" aria-label="Close">
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <input type="hidden" name="id_perusahaan">
                <label class="col-form-label col-lg-2 col-sm-12">Nomor Kontrak <span class="text-danger">*</span></label>
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <input type="text" class="form-control m-input" name="nomor_kontrak" placeholder="Masukan nomor kontrak" autocomplete="off">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-form-label col-lg-2 col-sm-12">Harga</label>
                <div class="col-lg-10 col-md-10 col-sm-12">
                    <input type="text" class="form-control m-input" name="harga_kontrak" placeholder="Masukan harga kontrak" autocomplete="off" data-toggle="m-tooltip" title="Masukan tarif/kWh, jika harga mengandung koma gunakan titim sebagai mengganti koma">
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions m-form__actions">
                <div class="row">
                    <div class="col-lg-12 ml-lg-auto">
                        <button type="submit" class="btn btn-success pull-right" id="saveKontrak"> <i class="la la-save mr-2"></i>Simpan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--end::Form-->
</div>

<div class="m-portlet m-portlet--success m-portlet--head-solid-bg" id="group-titik">
    <div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Titik
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
		<!--begin: Search Form -->
		<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
			<div class="row align-items-center">
				<div class="col-xl-12 m--align-right">
                    <button type="button" class="btn btn-brand m-btn m-btn--custom m-btn--icon m-btn--square m-btn--pill" onclick="tambahTitik()">
						<span>
							<i class="la la-plus"></i>
							<span>
								Tambah Titik
							</span>
						</span>
					</button>
				</div>
				<div class="col-xl-12 mt-4">
					<!-- <div class="m-separator m-separator--dashed d-xl-none"></div> -->
                    <!-- <div class="m_datatable list_titik" id="ajax_data"></div> -->
                    <table class="table table-striped table-bordered table-hover table-checkable" id="list_titik">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Merk</th>
                                <th>Type</th>
                                <th>Nomor</th>
                                <th>#</th>
                            </tr>
                        </thead>

                        <tbody></tbody>

                    </table>
                    <!--end: Datatable -->
				</div>
			</div>
		</div>
		<!--end: Search Form -->
		<!--begin: Datatable -->
		<!-- <div class="m_datatable list_titik" id="ajax_data"></div> -->
		<!--end: Datatable -->
	</div>
</div>

<!--begin::Modal Tambah titik-->
<div class="modal fade" id="modalTitik" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form id="formSaveTitik">
                <input type="hidden" name="id_perusahaan_titik">
                <div class="modal-body row">
                    <div class="col-lg-12">
                        <div class="form-group group-invalid">
                            <label for="nama_titik_pengukur" class="form-control-label">Nama :</label>
                            <input type="text" class="form-control" id="nama_titik_pengukur" name="nama_titik_pengukur">
                        </div>
                        <div class="form-group group-invalid">
                            <label for="caption_titik_pengukur" class="form-control-label">Keterangan :</label>
                            <input type="text" class="form-control" id="caption_titik_pengukur" name="caption_titik_pengukur">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <legend for="">1. Titik Pengukur</legend>
                        <hr class="mb-4">
                        <div class="form-group group-invalid">
                            <label for="merk_titik_pengukur" class="form-control-label">Merk :</label>
                            <input type="text" class="form-control" id="merk_titik_pengukur" name="merk_titik_pengukur">
                        </div>
                        <div class="form-group group-invalid">
                            <label for="type_titik_pengukur" class="form-control-label">Type :</label>
                            <input type="text" class="form-control" id="type_titik_pengukur" name="type_titik_pengukur">
                        </div>
                        <div class="form-group group-invalid">
                            <label for="nomor_titik_pengukur" class="form-control-label">Nomor :</label>
                            <input type="text" class="form-control" id="nomor_titik_pengukur" name="nomor_titik_pengukur">
                        </div>
                        <div class="form-group group-invalid">
                            <label for="faktor_kali_titik_pengukur" class="form-control-label">Faktor Kali :</label>
                            <input type="text" class="form-control" id="faktor_kali_titik_pengukur" name="faktor_kali_titik_pengukur">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <legend for="">2. Titik Pembanding</legend>
                        <hr class="mb-4">
                        <div class="form-group group-invalid">
                            <label for="merk_titik_pembanding" class="form-control-label">Merk :</label>
                            <input type="text" class="form-control" id="merk_titik_pembanding" name="merk_titik_pembanding">
                        </div>
                        <div class="form-group group-invalid">
                            <label for="type_titik_pembanding" class="form-control-label">Type :</label>
                            <input type="text" class="form-control" id="type_titik_pembanding" name="type_titik_pembanding">
                        </div>
                        <div class="form-group group-invalid">
                            <label for="nomor_titik_pembanding" class="form-control-label">Nomor :</label>
                            <input type="text" class="form-control" id="nomor_titik_pembanding" name="nomor_titik_pembanding">
                        </div>
                        <div class="form-group group-invalid">
                            <label for="faktor_kali_titik_pembanding" class="form-control-label">Faktor Kali :</label>
                            <input type="text" class="form-control" id="faktor_kali_titik_pembanding" name="faktor_kali_titik_pembanding">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary" id="saveTitik">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--end::Modal-->