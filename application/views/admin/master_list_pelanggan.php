<div class="m-portlet m-portlet--success m-portlet--head-solid-bg">
    <div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					List Pelanggan
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
		<!--begin: Search Form -->
		<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
			<div class="row align-items-center">
				<div class="col-xl-12 m--align-right">
                    <a href="<?= base_url(); ?>webbackend/master/create" class="btn btn-brand m-btn m-btn--custom m-btn--icon m-btn--square m-btn--pill">
                        <span>
							<i class="la la-plus"></i>
							<span>
								Tambah Titik
							</span>
						</span>
                    </a>
				</div>
				<div class="col-xl-12 mt-4">
                    <table class="table table-striped table-bordered table-hover table-checkable" id="list_pelanggan">
                        <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>Tlp</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>#</th>
                            </tr>
                        </thead>

                        <tbody></tbody>

                    </table>
                    <!--end: Datatable -->
				</div>
			</div>
		</div>
	</div>
</div>