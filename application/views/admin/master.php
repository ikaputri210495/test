<div class="m-portlet m-portlet--mobile" id="group-table">
	<div class="m-portlet__head">
		<div class="m-portlet__head-caption">
			<div class="m-portlet__head-title">
				<h3 class="m-portlet__head-text">
					Master Data Perusahaan
				</h3>
			</div>
		</div>
	</div>
	<div class="m-portlet__body">
		<!--begin: Search Form -->
		<div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
			<div class="row align-items-center">
				<div class="col-xl-8 order-2 order-xl-1">
					<div class="form-group m-form__group row align-items-center">
						<div class="col-md-4">
							<!-- <div class="m-input-icon m-input-icon--left">
								<input type="text" class="form-control m-input m-input--solid" placeholder="Search..." id="generalSearch">
								<span class="m-input-icon__icon m-input-icon__icon--left">
									<span>
										<i class="la la-search"></i>
									</span>
								</span>
							</div> -->
						</div>
					</div>
				</div>
				<div class="col-xl-4 order-1 order-xl-2 m--align-right">
                    <button type="button" class="btn btn-brand m-btn m-btn--custom m-btn--icon m-btn--square m-btn--pill" onclick="importExcelPelatihan()">
						<span>
							<i class="la la-file-excel-o"></i>
							<span>
								Import Data Excel
							</span>
						</span>
					</button>
					<div class="m-separator m-separator--dashed d-xl-none"></div>
				</div>
			</div>
		</div>
		<!--end: Search Form -->
		
        <!-- <div class="m_datatable absesnsi" id="ajax_data"></div> -->
        <table class="table table-striped- table-bordered table-hover table-checkable" id="perusahaan">
            <thead>
                <tr>
                    <th>Perusahaan</th>
                    <th>Alamat</th>
                    <th>Telphone</th>
                    <th>Email</th>
                </tr>
            </thead>
        </table>
		<!--end: Datatable -->
	</div>
</div>