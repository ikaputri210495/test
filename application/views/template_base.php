<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

<!-- begin::Head -->
<!-- Mirrored from keenthemes.com/metronic/preview/?page=index&demo=default by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 06 Sep 2018 05:14:37 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
	<meta charset="utf-8" />

	<title>SIREKA | <?=$title?></title>
	<meta name="description" content="Latest updates and statistic charts">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

	<!--begin::Web font -->
	<script src="<?= base_url(); ?>assets/vendors/custom/webfont/1.6.16/webfont.js"></script>
	<script>
		WebFont.load({
			google: {
				"families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
			},
			active: function () {
				sessionStorage.fonts = true;
			}
		});
	</script>
	<!--end::Web font -->
    
    <?php echo $assets_css; ?>
</head>
<!-- end::Head -->


<!-- begin::Body -->

<body
	class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
	<!-- begin:: Page -->
	<div class="m-grid m-grid--hor m-grid--root m-page">
		<!-- BEGIN: Header -->
        <?php echo $navbar; ?>
		<!-- END: Header -->
		<!-- begin::Body -->
		<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
			<!-- BEGIN: Left Aside -->
			<?php echo $sidebar; ?>
			<!-- END: Left Aside -->
			<div class="m-grid__item m-grid__item--fluid m-wrapper">
				<!-- BEGIN: Subheader -->
				<!-- <div class="m-subheader ">
					<div class="d-flex align-items-center">
						<div class="mr-auto">
							<h3 class="m-subheader__title ">Dashboard</h3>
						</div>
					</div>
				</div> -->
				<!-- END: Subheader -->
				<div class="m-content" style="background-image: url('<?=base_url()?>assets/img/s_courses_bg.jpg')">
                    <?php echo $content; ?>
				</div>
			</div>
		</div>
		<!-- end:: Body -->
		<?php
		if ($this->session->userdata('user_role') == 'root') { ?>
			<!--begin::Modal-->
			<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ganti Password</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<form id="formSaveNewPasswordAdmin">
							<div class="modal-body">
								<div class="modal-body">
									<div class="form-group group-invalid">
										<label for="new_password_admin" class="form-control-label">Password Baru :</label>
										<input type="password" class="form-control" id="new_password_admin" name="new_password_admin">
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary" id="saveNewPasswordAdmin">Save changes</button>
							</div>
						</form>
					</div>
				</div>
			</div>
			<!--end::Modal-->
		<?php
		}
		?>

		<!-- begin::Footer -->
		<?= $footer; ?>
		<!-- end::Footer -->


	</div>
	<!-- end:: Page -->

	
	<!-- begin::Scroll Top -->
	<div id="m_scroll_top" class="m-scroll-top">
		<i class="la la-arrow-up"></i>
	</div>
	<!-- end::Scroll Top -->
	
	<?= $assets_js; ?>

	<?php
	if ($this->session->userdata('user_role') == 'root') { ?>
		<script type="text/javascript">
			$(function(){
				$("#formSaveNewPasswordAdmin").validate({
					ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
					highlight: function(element, errorClass) {
						$(element).removeClass(errorClass);
					},
					unhighlight: function(element, errorClass) {
						$(element).removeClass(errorClass);
					},
					errorPlacement: function(error, element) {
						error.addClass('form-control-feedback text-danger');
						element.closest('.group-invalid').append(error);
					},
					rules: {
						new_password_admin: {
							required: !0
						}
					},
					submitHandler: function (e) {
						var formData = new FormData($('#formSaveNewPasswordAdmin')[0]);

						$.ajax({
							type: 'POST',
							url: site_url + 'webbackend/main/gani_password',
							data : formData,
							contentType : false,
							processData : false,
							cache: false,
							dataType : "JSON",
							success: function(data) {
								mApp.block("#formSaveNewPasswordAdmin", {
									overlayColor: "#000000",
									type: "loader",
									state: "primary",
									message: "Processing..."
								});
								if (data.msg == 'gagal') {
									$('#m_modal_1').modal('hide');
									toastr.error("Gagal!");            
								} else {
									setTimeout(function () {
										mApp.unblock("#formSaveNewPasswordAdmin");
										window.location.href = data.url;
									}, 2e3);
								}
							}
						});
					}
				})
			});
		</script>
	<?php
	}
	?>
</body>
<!-- end::Body -->

<!-- Mirrored from keenthemes.com/metronic/preview/?page=index&demo=default by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 06 Sep 2018 05:14:37 GMT -->

</html>