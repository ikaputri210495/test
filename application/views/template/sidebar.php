<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
    <i class="la la-close"></i>
</button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">
    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark" m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <li class="m-menu__item  m-menu__item<?= ($title == 'Dashboard') ? '--active' : ''  ?>" aria-haspopup="true">
                <a href="<?= base_url(); ?>webbackend/dashboard" class="m-menu__link">
                    <i class="m-menu__link-icon flaticon-line-graph"></i>
                    <span class="m-menu__link-title">
                        <span class="m-menu__link-wrap">
                            <span class="m-menu__link-text">Dashboard</span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="m-menu__item  m-menu__item--submenu <?= ($title == 'Tambah Pelanggan' || $title == 'List Pelanggan') ? 'm-menu__item--open m-menu__item--expanded' : ''  ?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                <a href="javascript:;"class="m-menu__link m-menu__toggle">
                    <i class="m-menu__link-icon flaticon-tool"></i>
                    <span class="m-menu__link-text">Master Data</span>
                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                </a>
                <div class="m-menu__submenu ">
                    <span class="m-menu__arrow"></span>
                    <ul class="m-menu__subnav">
                        <li class="m-menu__item  m-menu__item<?= ($title == 'Tambah Pelanggan') ? '--active' : ''  ?>" aria-haspopup="true">
                            <a href="<?= base_url(); ?>webbackend/master/create" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">Tambah Pelanggan</span>
                            </a>
                        </li>
                        <li class="m-menu__item  m-menu__item<?= ($title == 'List Pelanggan') ? '--active' : ''  ?>" aria-haspopup="true">
                            <a href="<?= base_url(); ?>webbackend/master/list_pelanggan" class="m-menu__link ">
                                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                <span class="m-menu__link-text">List Pelanggan</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
    <!-- END: Aside Menu -->
</div>