<!--begin::Global Theme Bundle -->
<script src="<?= base_url(); ?>assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
<!--end::Global Theme Bundle -->

<script type="text/javascript">
    var site_url = '<?php echo site_url(); ?>';
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "newestOnTop": false,
        "progressBar": false,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
</script>

<!-- add js function -->
<?php
if (isset($add_js)) {
    foreach ($add_js as $key) {
        echo "\n";
        echo '<script type="text/javascript" src="'.base_url($key).'"></script>';
    }
}
?>
<!-- /add js function -->