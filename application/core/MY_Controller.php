<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    function render_page($content, $data = NULL) {
        $user_id        = $this->session->userdata('user_id');
        $user_fullname  = $this->session->userdata('user_fullname');
        $user_role      = $this->session->userdata('user_role');
        $this->load->database();
        if (isset($user_id)) {
            $data['assets_css'] = $this->load->view('template/assets_css', $data, TRUE);
            $data['assets_js']  = $this->load->view('template/assets_js', $data, TRUE);
            $data['navbar']     = $this->load->view('template/navbar', $data, TRUE);
            if ($user_role == 'root') {
                $data['sidebar'] = $this->load->view('template/sidebar', $data, TRUE);
            } elseif ($user_role == 'user' || $user_role == 'pegawai') {
                // $date = date('Y-m-d');
                // $sql = "SELECT pelatihan_user.id AS id_pelatihan_user, pelatihan_index.id AS id_pelatihan FROM pelatihan_user JOIN pelatihan_index ON pelatihan_user.id_pelatihan = pelatihan_index.id WHERE pelatihan_user.id_user = '$user_id' AND pelatihan_user.status_approve = '2' AND pelatihan_index.pelaksanaan_pelatihan > '$date'";
                // $hasil = $this->db->query($sql);
                // $data['jmlA'] = $hasil->num_rows();
                // $data['sidebar'] = $this->load->view('template/sidebar_user', $data, TRUE);
            }
            $data['content']    = $this->load->view($content, $data, TRUE);
            $data['footer']     = $this->load->view('template/footer', $data, TRUE);
            $this->load->view('template_base', $data);
        } else {
            redirect('webbackend/login');
        }
    }

    // function render_page_user($content, $data = NULL) {
    //     $sql = "SELECT * FROM kontak ORDER BY id DESC";
    //     $hasil = $this->db->query($sql);
    //     $company = $hasil->row_array();
    //     $data['telepon']    = $company['telepon'];
    //     $data['email']      = $company['email'];
    //     $data['alamat']     = $company['alamat'];
    //     $data['facebook']   = $company['facebook'];
    //     $data['twitter']    = $company['twitter'];
    //     $data['instagram']  = $company['instagram'];
    //     $data['youtube']    = $company['youtube'];

    //     $data['assets_css'] = $this->load->view('template_page/assets_css', $data, TRUE);
    //     $data['assets_js']  = $this->load->view('template_page/assets_js', $data, TRUE);
    //     $data['navbar']     = $this->load->view('template_page/navbar', $data, TRUE);
    //     $data['footer']     = $this->load->view('template_page/footer', $data, TRUE);
    //     $data['content']    = $this->load->view($content, $data, TRUE);

    //     $this->load->view('template_base_user', $data);
    // }
}


?>