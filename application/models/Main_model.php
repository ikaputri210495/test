<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_model extends CI_Model {

    public function check_users($username) {
        $this->db->select('*');
        $this->db->where('username', $username);
        $query = $this->db->get('user');
        return $query;
    }

    public function check_user_by_id($id) {
        $this->db->select('*');
        $this->db->where('id', $id);
        $query = $this->db->get('user');
        return $query;
    }

}