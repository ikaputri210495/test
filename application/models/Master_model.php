<?php

class Master_model extends CI_Model
{
    public function list_data_pelanggan($id_perusahaan) {
        $this->db->select('*');
        $this->db->from('titik');
        $this->db->where('id_pelanggan', $id_perusahaan);
        $hasil = $this->db->get();
        return $hasil->result_array();
    }

    public function get_perusahaan($id_perusahaan) {
        $this->db->select('nama');
        $this->db->from('pelanggan');
        $this->db->where('id', $id_perusahaan);
        $hasil = $this->db->get();
        return $hasil->row_array();
    }

    public function list_data_customers() {
        $sql = "SELECT * FROM pelanggan ORDER BY nama ASC";
        $hasil = $this->db->query($sql);
        return $hasil->result_array();
    }
}


?>