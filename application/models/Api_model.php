<?php

class Api_model extends CI_Model
{
    public function cek_titik($id_pelanggan, $id_titik, $date) {
        $this->db->select('id');
        $this->db->from('history');
        $this->db->where('id_pelanggan', $id_pelanggan);
        $this->db->where('id_titik', $id_titik);
        $this->db->where("date_format(idt, '%Y-%m') =", $date);
        $hasil = $this->db->get();
        return $hasil;
    }

    public function cek_jumlah_titik_by_id_pelanggan($id_pelanggan) {
        $this->db->select('id');
        $this->db->from('titik');
        $this->db->where('id_pelanggan', $id_pelanggan);
        $hasil = $this->db->get();
        return $hasil;
    }

    public function cek_titik_periode_by_id_Pelanggan($id_pelanggan, $date) {
        $this->db->select('id');
        $this->db->from('history');
        $this->db->where('id_pelanggan', $id_pelanggan);
        $this->db->where("date_format(idt, '%Y-%m') =", $date);
        $hasil = $this->db->get();
        return $hasil;
    }

    public function get_pelanggan_by_id($id_pelanggan) {
        $this->db->from('pelanggan');
        $this->db->where('id', $id_pelanggan);
        $hasil = $this->db->get();
        return $hasil;
    }
}

?>