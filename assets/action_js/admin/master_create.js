var idPelanggan;
$(function(){
    $('#group-kontrak').hide();
    $('#group-titik').hide();

    $("#formPelanggan").validate({
        rules: {
            nama_perusahaan: {
                required: !0
            },
            email_perusahaan: {
                required: !0,
                email: true
            },
        },
        messages: {
            email_perusahaan: {
                email: 'Email tidak sesuai format dan tidak valid',
            }
        },
        invalidHandler: function (e, t) {
            $("#m_form_1_msg").removeClass("m--hide").show(), mUtil.scrollTo("m_form_1_msg", -200)
        },
        submitHandler: function (e) {
            swal({
                title: 'Anda yakin?',
                text: "Pastikan data yang sudah di inputkan benar!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Simpan',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then(function(result){
                if (result.value) {
                    $("#savePelanggan").attr("disabled", true);
                    var formData = new FormData($('#formPelanggan')[0]);

                    $.ajax({
                        url: site_url + "webbackend/master/insert_data_perusahaan",
                        type: "POST",
                        data : formData,
                        contentType : false,
                        processData : false,
                        dataType : "JSON",
                        success: function (data) {
                            if (data.msg == 'berhasil') {
                                $('#group-pelanggan').fadeOut("slow");
                                showInputKontrak();
                                idPelanggan = data.id_perusahaan;
                                $('[name=id_perusahaan]').val(data.id_perusahaan);
                                toastr.success("Berhasil menambahkan perusahaan!");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert('Error deleting data');
                        }
                    });
                }
            });
        }
    });

    $("#formKontrak").validate({
        rules: {
            nama_perusahaan: {
                required: !0
            },
            email_perusahaan: {
                required: !0,
                email: true
            },
        },
        messages: {
            email_perusahaan: {
                email: 'Email tidak sesuai format dan tidak valid',
            }
        },
        invalidHandler: function (e, t) {
            $("#m_form_2_msg").removeClass("m--hide").show(), mUtil.scrollTo("m_form_2_msg", -200)
        },
        submitHandler: function (e) {
            swal({
                title: 'Anda yakin?',
                text: "Pastikan data yang sudah di inputkan benar!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Simpan',
                cancelButtonText: 'Batal',
                reverseButtons: true
            }).then(function(result){
                if (result.value) {
                    $("#saveKontrak").attr("disabled", true);
                    var formData = new FormData($('#formKontrak')[0]);

                    $.ajax({
                        url: site_url + "webbackend/master/insert_data_kontrak",
                        type: "POST",
                        data : formData,
                        contentType : false,
                        processData : false,
                        dataType : "JSON",
                        success: function (data) {
                            if (data.msg == 'berhasil') {
                                $('#group-kontrak').fadeOut("slow");
                                showInputTitik();
                                toastr.success("Berhasil menambahkan kontrak perusahaan!");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert('Error deleting data');
                        }
                    });
                }
            });
        }
    });

    $("#formSaveTitik").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        highlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);
        },
        errorPlacement: function(error, element) {
            error.addClass('form-control-feedback text-danger');
            element.closest('.group-invalid').append(error);
        },
        rules: {
            nama_titik_pengukur: {
                required: !0
            },
            caption_titik_pengukur: {
                required: !0
            },
            merk_titik_pengukur: {
                required: !0
            },
            type_titik_pengukur: {
                required: !0
            },
            nomor_titik_pengukur: {
                required: !0
            }
        },
        submitHandler: function (e) {
            $("#saveTitik").attr("disabled", true);
            var formData = new FormData($('#formSaveTitik')[0]);

            $.ajax({
                type: 'POST',
                url: site_url + 'webbackend/master/insert_data_titik',
                data : formData,
                contentType : false,
                processData : false,
                cache: false,
                dataType : "JSON",
                success: function(data) {
                    if (data.msg == 'berhasil') {
                        $('#modalTitik').modal('hide');
                        toastr.success("Berhasil menambah titik baru!");
                        tableTitik();
                    }
                }
            });
        }
    });
});

function showInputKontrak() {
    $('#group-kontrak').fadeIn("slow");
    $("#progress-pross").css("width", "50%");
    $('#m_wizard_form_step_2').addClass('m-wizard__step--current');
}

function showInputTitik() {
    $('#group-titik').fadeIn("slow");
    $("#progress-pross").css("width", "100%");
    $('#m_wizard_form_step_3').addClass('m-wizard__step--current');
    tableTitik();
}

function tableTitik() {
    // $(".list_titik").mDatatable({
    //     destroy: true,
    //     data: {
    //         type: "remote",
    //         source: {
    //             read: {
    //                 // sample GET method
    //                 method: 'GET',
    //                 url: site_url + 'webbackend/master/data_table_titik/'+idPelanggan, //mdatatable
    //                 map: function(raw) {
    //                     // sample data mapping
    //                     var dataSet = raw;
    //                     if (typeof raw.data !== 'undefined') {
    //                       dataSet = raw.data;
    //                     }
    //                     return dataSet;
    //                 },
    //             },
    //         },
    //         pageSize: 10,
    //         serverPaging: false,
    //         serverFiltering: false,
    //         serverSorting: false,
    //     },
    //     layout: {
    //         theme: 'default',
    //         class: '',
    //         scroll: false,
    //         footer: false
    //     },
    //     // column sorting
    //     sortable: true,
    //     pagination: true,
    //     toolbar: {
    //         // toolbar items
    //         items: {
    //             // pagination
    //             pagination: {
    //                 // page size select
    //                 pageSizeSelect: [10, 20, 30, 50, 100],
    //             },
    //         },
    //     },
    //     search: {
    //         input: $('#generalSearchAbsensi'),
    //     },
    //     columns: [
    //         {
    //             field: 'nama_titik',
    //             title: 'Nama'
    //         },
    //     ]
    // })

    $('#list_titik').DataTable({
        destroy: true,
        // scrollY: "50vh",
        // scrollX: !0,
        // scrollCollapse: !0,
        ajax: {
            url: site_url + 'webbackend/master/data_table_titik/'+idPelanggan,
            dataSrc: ""
        },
        columns: [
            {data: "nama_titik"},
            {data: "merk"},
            {data: "type"},
            {data: "nomor"},
            {
                data: null,
                targets: -1,
                orderable: !1,
                render: function(row, a, i) {
                    var base = '\
                    <button type="button" onclick="delete(event,`'+row.id+'`)" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\
                        <i class="la la-trash-o"></i>\
                    </button>'

                    return base;
                }
            }
        ]
    });
}

function tambahTitik() {
    save_method = 'add';
    $('[name=id_perusahaan_titik]').val(idPelanggan);
    $('#formSaveTitik')[0].reset();
    $("#saveTitik").attr("disabled", false);
    $('.modal-title').text('Tambah Titik');
    $('#modalTitik').modal('show');
}