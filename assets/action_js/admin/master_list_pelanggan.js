function loadTable() {
    $('#list_pelanggan').DataTable({
        destroy: true,
        // scrollY: "50vh",
        // scrollX: !0,
        // scrollCollapse: !0,
        ajax: {
            url: site_url + 'webbackend/master/data_table_titik/'+idPelanggan,
            dataSrc: ""
        },
        columns: [
            {data: "nama_titik"},
            {data: "merk"},
            {data: "type"},
            {data: "nomor"},
            {
                data: null,
                targets: -1,
                orderable: !1,
                render: function(row, a, i) {
                    var base = '\
                    <button type="button" onclick="delete(event,`'+row.id+'`)" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Delete">\
                        <i class="la la-trash-o"></i>\
                    </button>'

                    return base;
                }
            }
        ]
    });
}